﻿using Ruleta.Data.Entities;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using static Ruleta.Data.Entities.Person;

namespace Ruleta.Methods
{
    public class MPerson
    {
        private readonly IDatabase _database;

        public MPerson(IDatabase database)
        {
            this._database = database;
        }
        public string Create(PersonModel model)
        {
            try
            {
                model.Id = DateTime.Now.ToString("mmhhyyyyssdd");
                string jsonString = JsonSerializer.Serialize(model);
                _database.StringSet("Person_" + model.Id, jsonString);
                return "successful";
            }
            catch (Exception ex)
            {
                return "denied";
                throw;
            }
        }

        public bool HasCredits(PersonModel model)
        {
            if (_database.KeyExists("person_"+model.Id))
            {
                PersonModel credits = JsonSerializer.Deserialize<PersonModel>(_database.StringGet(model.Id));
                if (credits.Credits != 0)
                {
                    return true;
                }
                return false;
            }
            return false;
        }


        public int Amount(PersonModel model)
        {
            if (_database.KeyExists("person_"+model.Id))
            {
                PersonModel credits = JsonSerializer.Deserialize<PersonModel>(_database.StringGet(model.Id));
                if (credits.Credits != 0)
                {
                    return credits.Credits;
                }
                return 0;
            }
            return 0;
        }

        public int Subtract(PersonModel model)
        {
            if (_database.KeyExists("Person_" + model.Id))
            {
                PersonModel credits = JsonSerializer.Deserialize<PersonModel>(_database.StringGet("Person_"+model.Id));
                if (credits.Credits != 0)
                {
                    int resta = credits.Credits- model.Credits;
                    if (resta < 0)
                    {
                        return  0;
                    }
                    string data = JsonSerializer.Serialize(model);
                    _database.KeyDelete(model.Id);
                    _database.StringSet("person_"+model.Id, data);
                    return resta;
                }
                return 0;
            }
            return 0;
        }

    }
}
