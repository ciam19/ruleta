﻿using Ruleta.Data.Entities;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using static Ruleta.Data.Entities.Bet;
using static Ruleta.Data.Entities.Person;

namespace Ruleta.Methods
{
    public class MBet
    {
        private readonly IDatabase _database;

        public MBet(IDatabase database)
        {
            this._database = database;
        }
        public string Tobet(betModel model)
        {
            MPerson MPerson = new MPerson(_database);
            PersonModel Person = new PersonModel();
            betModel bet = new betModel();
            Person.Id = model.IdPerson;
            Person.Credits = model.Credits;
            int rest = MPerson.Subtract(Person);
            if (rest != 0)
            {
                bet.IdPerson = Person.Id;
                bet.Credits = rest;
                bet.RouletteId = model.RouletteId;
                SetBet(bet);
                return "successful";
            }
            return "denied";
        }

        public string SetBet(betModel model)
        {
            model.betId = DateTime.Now.ToString("mmhhyyyyssdd");
            if (!_database.KeyExists("Bet_" + model.betId))
            {
                var data = JsonSerializer.Serialize(model);
                if (_database.StringSet("Bet_" + model.betId, data))
                {
                    return "succesful";
                }
                return "denied";
            }
            return "succesful";
        }
    }
}
