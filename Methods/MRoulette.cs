﻿using Ruleta.Data.Entities;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using static Ruleta.Data.Entities.HistoryBet;
using static Ruleta.Data.Entities.Roulette;

namespace Ruleta.Methods
{
    public class MRoulette
    {
        private readonly IDatabase _database;

        public MRoulette(IDatabase database)
        {
            this._database = database;
        }

        public string Open(rouletteModel model)
        {
            if (_database.KeyExists("Roulette_"+model.Id))
            {
                string data = _database.StringGet("Roulette_" + model.Id);
                rouletteModel result = JsonSerializer.Deserialize<rouletteModel>(data);
                result.Status = 1;
                string jsonString = JsonSerializer.Serialize(result);
                _database.KeyDelete("Roulette_" + model.Id);
                _database.StringSet("Roulette_" + model.Id, jsonString);
                return "successful";
            }
            return "denied";
        }


        public bool Status(rouletteModel model)
        {
            model.Id = DateTime.Now.ToString("mmhhyyyyssdd");
            if (_database.KeyExists("Roulette_" + model.Id))
            {
                rouletteModel roulette = JsonSerializer.Deserialize<rouletteModel>
                                        (_database.StringGet("Roulette_" + model.Id));
                if (roulette.Status == 0)
                {
                    return false;
                }
                return true;
            }
            return false;
        }

        public int Lounch(rouletteModel model)
        {
            List<int> listHistory = new List<int>();
            int thrown = new Random().Next(0, 36);
            if (_database.KeyExists("History_" + model.Id))
            {
                RouletteHistory Historyl = JsonSerializer.Deserialize<RouletteHistory>
                                          (_database.StringGet("History_" + model.Id));
                Historyl.Historical.Add(thrown);
                string datal = JsonSerializer.Serialize(Historyl);
                _database.KeyDelete("History_" + model.Id);
                _database.StringSet("History_" + model.Id, datal);
                return thrown;
            }
            RouletteHistory History = new RouletteHistory();
            listHistory.Add(thrown);
            History.IdRoulette = model.Id;            
            History.Historical = listHistory;
            string data = JsonSerializer.Serialize(History);
            _database.StringSet("History_" + model.Id, data);
            return thrown;
        }
    }
}
