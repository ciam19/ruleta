﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Ruleta.Methods;
using StackExchange.Redis;
using static Ruleta.Data.Entities.Person;

namespace Ruleta.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonController : ControllerBase
    {
        private readonly IDatabase _database;

        public PersonController(IDatabase database)
        {
            this._database = database;
        }

        [HttpPost]
        public string Post(PersonModel model)
        {
            MPerson Person = new MPerson(_database);
            return Person.Create(model);
        }

    }
}
