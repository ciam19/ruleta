﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Ruleta.Data.Entities;
using Ruleta.Methods;
using StackExchange.Redis;
using static Ruleta.Data.Entities.Bet;

namespace Ruleta.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BetController : ControllerBase
    {
        private readonly IDatabase _database;

        public BetController(IDatabase database)
        {
            this._database = database;
        }

        [HttpPost]
        public string Post(betModel model)
        {
            MBet bet = new MBet(_database);
            return bet.Tobet(model);
        }
    }
}
