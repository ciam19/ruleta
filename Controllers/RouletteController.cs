﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Ruleta.Methods;
using StackExchange.Redis;
using static Ruleta.Data.Entities.Roulette;

namespace Ruleta.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RouletteController : ControllerBase
    {
        private readonly IDatabase _database;

        public RouletteController(IDatabase database)
        {
            this._database = database;
        }

        [HttpGet]
        public string NewRoulette()
        {
            rouletteModel rulett = new rouletteModel();
            rulett.Id = DateTime.Now.ToString("mmhhyyyyssdd");
            string jsonString = JsonSerializer.Serialize(rulett);
            if (_database.StringSet("Roulette_"+rulett.Id, jsonString))
            {
                return rulett.Id;
            }
            else
            {
                return "0";
            }
        }

        [HttpPost]
        public string OpenRoulette(rouletteModel model)
        {
            MRoulette Roulette = new MRoulette(_database);
            return Roulette.Open(model);
        }

        [HttpGet]
        [Route("Toturn/{Id}")]
        public int Toturn(string id)
        {
            rouletteModel RouletteModel = new rouletteModel();
            RouletteModel.Id = id;
            MRoulette Roulette = new MRoulette(_database);
            return Roulette.Lounch(RouletteModel);
        }

    }
}
