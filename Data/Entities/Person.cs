﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ruleta.Data.Entities
{
    public class Person
    {
        public class PersonModel
        {
            public string Id { get; set; }
            public string Nombre { get; set; }
            public string Apellido { get; set; }
            public string Estado { get; set; }
            public int Credits { get; set; }
        }
    }
}
