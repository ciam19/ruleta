﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Ruleta.Data.Entities
{
    public class Bet
    {
        public class betModel
        {
            [Range(0, 36, ErrorMessage = "only numbers from 1 to 36")]
            public int Numbers { get; set; }

            //1-red 2-black
            public int Color { get; set; }
            public string IdPerson { get; set; }

            public string betId { get; set; }

            [Range(0, 1000, ErrorMessage = "you can only bet from 0 to 1000 dollars")]
            public int Credits { get; set; }
            public string RouletteId { get; set; }
        }
    }
}
