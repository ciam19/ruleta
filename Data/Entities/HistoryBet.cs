﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ruleta.Data.Entities
{
    public class HistoryBet
    {
        public class RouletteHistory
        {
            public string IdRoulette { get; set; }
            public List<int> Historical { get; set; }
            //public List<HistoryPerson> Historical { get; set; }
        }

        //public class HistoryPerson
        //{
        //    public int Number { get; set; }
        //    public List<int> PersonId { get; set; }
        //}
    }
}
